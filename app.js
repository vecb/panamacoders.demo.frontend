/* global angular */
'use strict';

angular.module('app.main',['ui.router','angular-loading-bar', 'ngAnimate',
    'app.nav'
    ])
    .config(['$stateProvider', '$urlRouterProvider',routeConfig])
    .constant('apiServiceBase','http://paback.nexsyscomputing.com/')

function routeConfig($stateProvider, $urlRouterProvider)
{
    $urlRouterProvider.otherwise('/home');
	
	$stateProvider
	.state('home', {
         url: '/home',
         templateUrl: './components/home/homeTemplate.html',
         controller: 'HomeController',
		 controllerAs:'home'
     })

	.state('items', {
         url: '/items',
         templateUrl: './components/items/itemsTemplate.html',
         controller: 'ItemsController',
		 controllerAs:'items'
     })
	 

}