/* global angular */
'use strict';

angular.module('app.main')
    .controller('ItemsController', ['itemsService',ItemsController])
	
function ItemsController(itemsService)
{
	var _this = this;
	this.addItem = _addItem;
	this.deleteItem = _deleteItem;
	this.updateItem = _updateItem;
	
	//promise.then(successCallback,errorCallback)
	itemsService.get()
		.then(
			function (response){
				_this.list = response.data;
				_this.errorMessage = null;
		},
			function (errorResponse){
				console.log("ERROR",errorResponse);
				_this.errorMessage = "Hubo un error leyendo la lista de items ("+errorResponse.status+" - "+errorResponse.statusText+")";
		});
	
	function _addItem(){
		var item= {
			Name: _this.editItem.Name
		};
		
		itemsService.post(item)
			.then(
				function(response){
					_this.list.push(response.data);
					_this.editItem = null;
					_this.errorMessage = null;
			},
				function (errorResponse){
					console.log("ERROR",errorResponse);
					_this.errorMessage = "Hubo un error agregando el item ("+errorResponse.status+" - "+errorResponse.statusText+")";
			});
	}
	
	function _updateItem(item){
		itemsService.put(item)
			.then(
				function(){
					_this.errorMessage = null;				
			},
				function (errorResponse){
					console.log("ERROR",errorResponse);
					_this.errorMessage = "Hubo un error guardando el item ("+errorResponse.status+" - "+errorResponse.statusText+")";
			});
	}
	
	function _deleteItem(item){
		itemsService.delete(item)
			.then(
				function(){
					_this.list.remove(function(elem){
						return elem.ID === item.ID
					});
					_this.errorMessage = null;
			},
			function (errorResponse){
				console.log("ERROR",errorResponse);
				_this.errorMessage = "Hubo un error borrando el item ("+errorResponse.status+" - "+errorResponse.statusText+")";
			});
	}
	
	
}