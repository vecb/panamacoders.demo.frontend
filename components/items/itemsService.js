/* global angular */
'use strict';

angular.module('app.main')
    .factory('itemsService', ['$http','apiServiceBase',itemsService])
	
function itemsService($http,apiServiceBase)
{
	var factory = {};
    factory.get = _get;
    factory.post = _post;
    factory.put = _put;
    factory.delete = _delete;
    
    function _get(){
        return $http.get(apiServiceBase+"api/items");
    };
    
    function _post(data){
        return $http.post(apiServiceBase+"api/items",data);
    };
    
    function _put(data){
        console.log("put",data);
        return $http.put(apiServiceBase+"api/items",data);
    };
    
    var config = {
        headers: {
            Authorization: "bearer WZnWKldi42neQBVzx7yHChWA4gpG4xGTFGCZuFDPxQIIBid-NYuvTgfGDNYOwfWs43EUlJNozdRcfmAmwUI0ayqfwHAIiwuRb_FCH9q98E7hSApy_rWsvApopn41Kc2i86TBYDopIIdNO9O5Scwhd6NIy1_rXD1XzAOYaMs1oIqf8DAMvRbXKUQdupx1-9P6_JQrXK-WKOAXtTL6ocjwpuYrDFPFdTDQV3Xsm__WJ6l6UD-MMESl-Bm8v4vBwTZYiK_jGEm5yvVr8x-L9_V5R0rmGyBAo5M5i-Ih3gZTi5yAhDDU-_lYTYQQ0xU2t7U_LsueQgJACI75yJShbEjs6vm6fd4y0yKVNbq827M_G6Dbn7xeNYmuzl5u8K564VLitWsZyegP3Y-vD7V_6lUNQkwuqaDqqBCforNawytvDt96qe8rhZwVLaEg2TME0tt-bhgQdOZBJVPTS3A_Ft_gp1cLlTYRHODhzU7DD63VqGo"
        }
    };
    
    function _delete(data){      
        return $http.delete(apiServiceBase+"api/items/"+data.ID,config);
    };
 
    return factory;
}