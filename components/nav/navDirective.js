/* global angular */
'use strict';

angular.module('app.nav')
    .directive('myNav', navDirective)

function navDirective ()
{
    return {
        templateUrl: './components/nav/navTemplate.html',
        controller: 'NavController'
    }
}



